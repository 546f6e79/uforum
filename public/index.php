<?php

/**
 * Everything is relative to the application root now.
 */
chdir(dirname(__DIR__));

// Decline static file requests back to the PHP built-in webserver
if (php_sapi_name() === 'cli-server' && is_file(__DIR__ . parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH))) {
	return false;
}

// Setup autoloading
if (file_exists('vendor/autoload.php')) {
	$loader = require 'vendor/autoload.php';
}

// Check autoloader
if (!$loader instanceof \Composer\Autoload\ClassLoader) {
	throw new RuntimeException('Unable to setup autoloader. Run `composer install`.');
}

// Configuration
// Load configuration from config path
$config = [];
foreach (array_diff(scandir('config'), ['.','..']) as $file) {
	if (substr($file, -4) !== '.php') {
		continue;
	}
	$config = array_merge_recursive($config, require 'config/' . $file);
}

// Build container and inject config
$container = new \Uforum\Core\Container($config['dependencies']);
$container->setService('config', $config);

// Run the application
$router   = new \Uforum\Core\Router($container);
echo $router->route(new \Uforum\Core\Request());
