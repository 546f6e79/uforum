<?php

return [
	'routes' => [
		'[/[:page]]' => [
			'controller' => \Uforum\Controllers\TopicController::class,
			'method' => 'topicList',
			'params' => [
				'page' => 'number',
			],
			'defaults' => [
				'page' => 1
			]
		],
		'/topic/show/:id[/[:page]]' => [
			'controller' => \Uforum\Controllers\TopicController::class,
			'method' => 'showTopic',
			'params' => [
				'id' => 'number',
				'page' => 'number',
			],
			'defaults' => [
				'page' => 1
			]
		],
		'/topic/create' => [
			'controller' => \Uforum\Controllers\TopicController::class,
			'method' => 'createTopic',
		],
		'/topic/post/create' => [
			'controller' => \Uforum\Controllers\TopicController::class,
			'method' => 'createPost',
		],
	],
];