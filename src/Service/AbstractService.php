<?php

namespace Uforum\Service;

use PDO;

abstract class AbstractService {
	public $db;

	public function __construct(PDO $db) {
		$this->db = $db;
	}

}