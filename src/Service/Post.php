<?php
namespace Uforum\Service;

use Uforum\Core\FilteredMap;
use Uforum\Entity\Post as PostEntity;
use Uforum\Exceptions\NotFoundException;
use PDO;

class Post extends AbstractService {

	public function getPostsByTopicId(int $topic_id, int $start, int $pageLength) : array {
		$query = 'SELECT * FROM `post` WHERE `topic_id` = :topic_id LIMIT :page, :length';
		$sth = $this->db->prepare($query);
		$sth->bindParam('topic_id', $topic_id, PDO::PARAM_INT);
		$sth->bindParam('page', $start, PDO::PARAM_INT);
		$sth->bindParam('length', $pageLength, PDO::PARAM_INT);
		$sth->execute();
		return $sth->fetchAll(PDO::FETCH_CLASS, PostEntity::class);
	}

	public function getCountPostsByTopicId(int $topic_id) : array {
		$query = 'SELECT COUNT(*) FROM `post` WHERE `topic_id` = :topic_id';
		$sth = $this->db->prepare($query);
		$sth->bindParam('topic_id', $topic_id, PDO::PARAM_INT);
		$sth->execute();
		return $sth->fetchColumn();
	}

	public function getCountPostsByUserId(int $user_id) : int {
		$query = 'SELECT COUNT(*) FROM `post` WHERE `user_id` = :user_id';
		$sth = $this->db->prepare($query);
		$sth->bindParam('user_id', $user_id, PDO::PARAM_INT);
		$sth->execute();
		return $sth->fetchColumn();
	}

	public function getLastPostByTopicId(int $topic_id) : int {
		$query = <<<SQL
SELECT `p`.*, `u`.`name` as author
FROM `post` p 
INNER JOIN `user` u ON `u`.`id` = `p`.`user_id`
WHERE
	`p`.`topic_id` = :topic_id 
SQL;
		$sth = $this->db->prepare($query);
		$sth->bindParam('topic_id', $topic_id, PDO::PARAM_INT);
		$sth->execute();
		$posts = $sth->fetchAll(PDO::FETCH_CLASS, PostEntity::class);
		if (empty($posts)) {
			throw new NotFoundException();
		}
		return $posts[0];
	}
	
	public function getPostsByTopic(int $topic_id, int $start = 0, int $pageLength = 10) :array {
		$query = <<<SQL
SELECT 
	`p`.*, 
	`u`.`name` as author,
	(SELECT COUNT(*) FROM `post` WHERE `user_id` = `u`.`id`) as count_posts_user
FROM `post` p
INNER JOIN `user` u ON `u`.`id` = `p`.`user_id`
WHERE 
	p.topic_id = :topic_id
ORDER BY p.created_at ASC
LIMIT :page, :length
SQL;
		$sth = $this->db->prepare($query);
		$sth->bindParam('topic_id', $topic_id, PDO::PARAM_INT);
		$sth->bindParam('page', $start, PDO::PARAM_INT);
		$sth->bindParam('length', $pageLength, PDO::PARAM_INT);
		$sth->execute();
		return $sth->fetchAll(PDO::FETCH_CLASS, PostEntity::class);
	}

	public function getCountPostsByTopic(int $topic_id) : int{
		$query = <<<SQL
SELECT COUNT(*)
FROM post
WHERE 
	post.topic_id = :topic_id
SQL;
		$sth = $this->db->prepare($query);
		$sth->bindParam('topic_id', $topic_id, PDO::PARAM_INT);
		$sth->execute();
		return $sth->fetchColumn();
	}

	public function getLastPostByTopic(int $topic_id) : \Uforum\Entity\Post {
		$query = <<<SQL
SELECT `post`.*, u.name as author
FROM `post`
INNER JOIN `user` u ON u.id = post.user_id
WHERE 
	`post`.`topic_id` = :topic_id
ORDER by `post`.`created_at` DESC
SQL;
		$sth = $this->db->prepare($query);
		$sth->bindParam('topic_id', $topic_id, PDO::PARAM_INT);
		$sth->execute();
		$posts = $sth->fetchAll(PDO::FETCH_CLASS, PostEntity::class);
		if (empty($posts)) {
			$post = new \Uforum\Entity\Post();
			$post->setAuthor('');
			return $post;
		}
		return $posts[0];
	}


	public function addNewPost(FilteredMap $data, int $user_id) : int {
		$query = <<<SQL
INSERT INTO `post` 
(`user_id`,`topic_id`,`name`,`description`,`created_at`) 
VALUES
(:user_id,:topic_id,:name,:description,:created_at)
SQL;
		$sth = $this->db->prepare($query);
		$sth->execute([
			'user_id' => $user_id,
			'topic_id' => $data->get('topic_id'),
			'name' => $data->get('name'),
			'description' => $data->get('description'),
			'created_at' => date('Y-m-d H:i:s')
		]);
		return $this->db->lastInsertId();
	}

}