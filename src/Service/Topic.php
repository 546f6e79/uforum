<?php
namespace Uforum\Service;

use Uforum\Core\FilteredMap;
use Uforum\Entity\Topic as TopicEntity;
use Uforum\Exceptions\NotFoundException;
use PDO;

class Topic extends AbstractService {

	/**
	 * @param int $topicId
	 * @return TopicEntity
	 * @throws NotFoundException
	 */
	public function getTopic(int $topicId): TopicEntity {
		$query = <<<SQL
SELECT t.*, `u`.`name` as author
FROM `topic` t
INNER JOIN `user` u ON `u`.`id` = `t`.`user_id`
WHERE 
	t.`id` = :id
SQL;
		$sth = $this->db->prepare($query);
		$sth->execute(['id' => $topicId]);
		$topics = $sth->fetchAll(PDO::FETCH_CLASS, TopicEntity::class);
		if (empty($topics)) {
			throw new NotFoundException();
		}
		return $topics[0];
	}

	public function getCount(): int {
		$query = 'SELECT COUNT(*) FROM `topic`';
		$sth = $this->db->query($query);
		return $sth->fetchColumn();
	}

	public function getCountByUserId(int $user_id): int {
		$query = 'SELECT COUNT(*) FROM `topic` WHERE `user_id` = :user_id';
		$sth = $this->db->prepare($query);
		$sth->execute(['user_id' => $user_id]);
		return $sth->fetchColumn();
	}

	public function getAll(int $start, int $pageLength): array {
		$query = <<<SQL
SELECT `t`.*, `u`.`name` as author
FROM `topic` t
INNER JOIN `user` u ON `u`.`id` = `t`.`user_id`
LIMIT :page, :length
SQL;

		$sth = $this->db->prepare($query);
		$sth->bindParam('page', $start, PDO::PARAM_INT);
		$sth->bindParam('length', $pageLength, PDO::PARAM_INT);
		$sth->execute();
		return $sth->fetchAll(PDO::FETCH_CLASS, TopicEntity::class);
	}

	public function addNewTopic(FilteredMap $data, int $user_id) : int {
		$query = <<<SQL
INSERT INTO `topic` 
(`user_id`,`name`,`description`,`created_at`) 
VALUES
(:user_id,:name,:description,:created_at)
SQL;
		$sth = $this->db->prepare($query);
		$sth->execute([
			'user_id' => $user_id,
			'name' => $data->get('name'),
			'description' => $data->get('description'),
			'created_at' => date('Y-m-d H:i:s')
		]);
		return $this->db->lastInsertId();
	}

}