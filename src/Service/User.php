<?php
namespace Uforum\Service;

use Uforum\Entity\User as UserEntity;
use Uforum\Exceptions\NotFoundException;
use PDO;

class User extends AbstractService {
	/**
	 * @param int $userId
	 * @return UserEntity
	 * @throws NotFoundException
	 */
	public function getById(int $userId): UserEntity {
		$query = 'SELECT `id`, `name` FROM `user` WHERE `id` = :user';
		$sth = $this->db->prepare($query);
		$sth->execute(['user' => $userId]);
		$users = $sth->fetchAll(PDO::FETCH_CLASS, UserEntity::class);

		if (empty($users)) {
			throw new NotFoundException();
		}

		return $users[0];
	}

	/**
	 * @param string $name
	 * @return UserEntity
	 * @throws NotFoundException
	 */
	public function getAndInsertByName(string $name): UserEntity {
		$query = 'SELECT `id`, `name` FROM `user` WHERE `name` = :user';
		$sth = $this->db->prepare($query);
		$sth->execute(['user' => $name]);
		$users = $sth->fetchAll(PDO::FETCH_CLASS, UserEntity::class);

		if (empty($users)) {
			$query = 'SELECT `id`, `name` FROM `user` WHERE `name` = :user';
			$sth = $this->db->prepare($query);
			$sth->execute(['user' => $name]);
			$users = $sth->fetchAll(PDO::FETCH_CLASS, UserEntity::class);
		}

		return $users[0];
	}

	public function getByName(string $name) : UserEntity {
		$query = 'SELECT `id`, `name` FROM `user` WHERE `name` = :user';
		$sth = $this->db->prepare($query);
		$sth->execute(['user' => $name]);
		$users = $sth->fetchAll(PDO::FETCH_CLASS, UserEntity::class);
		if(empty($users)){
			return $this->insertByName($name);
		}
		return $users[0];
	}

	public function insertByName(string $name) : UserEntity {
		$query = <<<SQL
INSERT INTO `user` 
  (`name`, `created_at`) 
VALUES
  (:name, :created_at)
SQL;
		$sth = $this->db->prepare($query);
		$sth->execute([
			'name' => $name,
			'created_at' => date('Y-m-d H:i:s')
		]);

		return $this->getByName($name);
	}
}