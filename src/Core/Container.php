<?php
namespace Uforum\Core;

use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;

/**
 * Simple DependecyInjection
 * Class Container
 * @package Uforum\Core
 */
class Container implements ContainerInterface {
	/**
	 * @var array
	 */
	protected $services = [];

	/**
	 * @var array
	 */
	protected $factories = [];

	/**
	 * Constructor.
	 *
	 * See {@see \Zend\ServiceManager\ServiceManager::configure()} for details
	 * on what $config accepts.
	 *
	 * @param array $config
	 */
	public function __construct(array $config = []) {
		foreach ($config['factories'] as $name => $factory) {
			$this->factories[$name] = new $factory;
		}
		foreach ($config['invokables'] as $name => $class) {
			$this->factories[$name] = $class;
		}
	}

	public function setService($name, $service) {
		$this->services[$name] = $service;
	}

	/**
	 * Finds an entry of the container by its identifier and returns it.
	 *
	 * @param string $name Identifier of the entry to look for.
	 *
	 * @throws \Interop\Container\Exception\NotFoundException  No entry was found for this identifier.
	 * @throws ContainerException Error while retrieving the entry.
	 * @throws \Exception
	 *
	 * @return mixed Entry.
	 */
	public function get($name) {
		// If service already initialized
		if (isset($this->services[$name])) {
			return $this->services[$name];
		}

		// If factory not found
		if (!isset($this->factories[$name])) {
			throw new \Exception($name . ' dependency not found.');
		}

		// If it is factory
		if (is_callable($this->factories[$name])) {
			return $this->services[$name] = $this->factories[$name]($this);
		}

		// If it is invokable
		if (is_string($this->factories[$name])) {
			return $this->services[$name] = new $this->factories[$name];
		}

		throw new \Exception('Can not initialize service');
	}

	/**
	 * Returns true if the container can return an entry for the given identifier.
	 * Returns false otherwise.
	 *
	 * @param string $name Identifier of the entry to look for.
	 *
	 * @return boolean
	 */
	public function has($name) {
		return isset($this->services[$name]) || isset($this->factories[$name]);
	}
}
