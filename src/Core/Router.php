<?php
namespace Uforum\Core;

use Interop\Container\ContainerInterface;
use Uforum\Controllers\ErrorController;

class Router {
	private $container;
	private $routeMap;
	private static $regexPatterns = ['number' => '\d+', 'string' => '\w+'];

	public function __construct(ContainerInterface $container) {
		$this->container = $container;
		$this->routeMap  = $this->container->get('config')['routes'];
	}

	public function route(Request $request): string {
		$path = $request->getPath();

		foreach ($this->routeMap as $route => $info) {
			if (empty($info['params'])) {
				$info['params'] = [];
			}
			$regexRoute = $this->getRegexRoute($route, $info['params']);

			$matches = [];
			if (preg_match($regexRoute, $path, $matches)) {
				$params = [];
				foreach ($info['params'] as $name => $type) {
					if ($matches[$name]) {
						$params[$name] = $matches[$name];
						continue;
					}
					$params[$name] = isset($info['defaults'][$name]) ? $info['defaults'][$name] : null;
				}

				return $this->executeController($params, $info, $request);
			}
		}

		$errorController = new ErrorController($this->container, $request);
		return $errorController->notFound();
	}

	private function getRegexRoute(string $route, array $params = []): string {
		$search  = ['[', ']'];
		$replace = ['(', ')?'];
		foreach ($params as $name => $type) {
			$search[]  = ':' . $name;
			$replace[] = '(?<' . $name . '>' . self::$regexPatterns[$type] . ')';
		}
		return '#^' . str_replace($search, $replace, $route) . '$#';
	}

	private function executeController(array $params , array $info, Request $request): string {
		$controllerName = $info['controller'];
		$controller     = new $controllerName($this->container, $request);

		if ($request->getCookies()->has('user')) {
			$userId = $request->getCookies()->get('user');
			$controller->setUserId($userId);
		}

		return call_user_func_array([$controller, $info['method']], $params);
	}
}