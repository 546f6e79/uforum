<?php

namespace Uforum\Helper;

class Paginator{

	/**
	 * @param string $list_class Css class
	 * @param int $page Current page number
	 * @param int $totalItems Total items for pagination
	 * @param string $route Route path before '/:page'
	 * @param int $page_length Items limit per page
	 * @param int $links_limit Limit visible links
	 * @return string
	 */
	public function createLinks(string $list_class, int $page, int $totalItems, string $route = '', int $page_length = 10, int $links_limit = 7) : string {
		$last = ceil($totalItems / $page_length);

		$start = (($page - $links_limit) > 0) ? $page - $links_limit : 1;
		$end = (($page + $links_limit) < $last) ? $page + $links_limit : $last;

		$html = '<ul class="' . $list_class . '">';

		if($page > $start){
			$class = ($page == 1) ? "disabled" : "";
			$href = ($class != 'disabled') ? $route.'/'.($page - 1) : 'javascript:void(0)';
			$html .= '<li class="' . $class . '"><a href="'.$href.'">&laquo;</a></li>';
		}

		if ($start > 1) {
			$html .= '<li><a href="'.$route.'/1">1</a></li>';
			$html .= '<li class="disabled"><span>...</span></li>';
		}

		for ($i = $start; $i <= $end; $i++) {
			$class = ($page == $i) ? "active" : "";
			$html .= '<li class="' . $class . '"><a href="'.$route.'/'. $i . '">' . $i . '</a></li>';
		}

		if ($end < $last) {
			$html .= '<li class="disabled"><span>...</span></li>';
			$html .= '<li><a href="'.$route.'/'.$last.'">'.$last.'</a></li>';
		}

		if($page < $end){
			$class = ($page == $last) ? "disabled" : "";
			$href = ($class != 'disabled') ? $route.'/'.($page + 1) : 'javascript:void(0)';
			$html .= '<li class="'.$class.'"><a href="'.$href.'">&raquo;</a></li>';
		}
		$html .= '</ul>';

		return $html;
	}
}