<?php

namespace Uforum\Controllers;

class TopicController extends AbstractController {

	/**
	 * Forum index action
	 *
	 * @param int $page Page number
	 * @param array $errors Some errors for form
	 * @return string template
	 */
	public function topicList(int $page, array $errors = []): string {
		$topicModel  = $this->di->get('Topic');
		$postModel   = $this->di->get('Post');
		$paginator   = $this->di->get('Paginator');
		$page_length = $this->di->get('config')['page_length'];
		$links_limit = $this->di->get('config')['links_limit'];

		$topicsCount = $topicModel->getCount();

		if($page){
			$start = ((int)$page - 1) * $page_length;
		}else{
			$start = 0;
		}

		$pagination  = $paginator->createLinks('pagination pagination-sm', $page, $topicsCount, '', $page_length, $links_limit);

		$topics      = $topicModel->getAll($start, $page_length);
		foreach ($topics as $key=>$topic){
			$posts_count = $postModel->getCountPostsByTopic($topic->getId());
			$last_post = $postModel->getLastPostByTopic($topic->getId());

			$topics[$key]->setPostsCount($posts_count);
			$topics[$key]->setLastPost($last_post);
		}

		$properties  = [
			'topics'     => $topics,
			'pagination' => $pagination,
			'errors'     => $errors
		];

		return $this->render('topic/topic_list', $properties);
	}

	/**
	 * Show topic page by topic_id
	 * @param int $topicId
	 * @param int $page Page number
	 * @param array $errors Some errors for form
	 * @return string template
	 */
	public function showTopic(int $topicId, int $page, array $errors = []): string {
		$topicModel  = $this->di->get('Topic');
		$postModel   = $this->di->get('Post');
		$paginator   = $this->di->get('Paginator');
		$page_length = $this->di->get('config')['page_length'];
		$links_limit = $this->di->get('config')['links_limit'];

		try {
			$topic = $topicModel->getTopic($topicId);
			$posts = $postModel->getPostsByTopic($topicId);
			$posts_count = $postModel->getCountPostsByTopic($topic->getId());
			$route = '/topic/show/'.$topicId;
			$pagination = $paginator->createLinks('pagination pagination-sm', $page, $posts_count, $route, $page_length, $links_limit );

			$topic->setPostsCount($posts_count);
		} catch (\Exception $e) {
			$properties = ['errorMessage' => 'Topic not found!'];
			return $this->render('error', $properties);
		}

		$properties = [
			'topic'      => $topic,
			'posts'      => $posts,
			'pagination' => $pagination,
			'page'       => $page,
			'errors'     => $errors,
		];

		return $this->render('topic/topic', $properties);
	}

	/**
	 * Create new Topic
	 *
	 * @return string Go back with errors or redirect to new topic
	 */
	public function createTopic(){
		$topicModel  = $this->di->get('Topic');
		$userModel   = $this->di->get('User');

		$data     = $this->request->getParamsRawArray();

		$errors = [];
		foreach ($data as $key => $val) {
			$val = htmlspecialchars( trim($val) );
			if(empty($val)){
				$errors[$key] = $key.' is empty.';
			}
			$data[$key] = htmlspecialchars(trim($val));
		}

		if(!empty($errors)){
			return $this->topicList(1, $errors);
		}

		$userName = htmlspecialchars( trim($data['author']) );
		$user = $userModel->getByName($userName);

		$new_topic = $topicModel->addNewTopic($this->request->getParams(), $user->getId());

		header('Location: /topic/show/'.$new_topic);
	}

	/**
	 * Create new Post for Topic
	 *
	 * @return string Go back with errors or redirect to current page(reload)
	 */
	public function createPost(){
		$postModel  = $this->di->get('Post');
		$userModel  = $this->di->get('User');

		$data       = $this->request->getParamsRawArray();

		$errors = [];
		foreach ($data as $key => $val) {
			$val = htmlspecialchars( trim($val) );
			if(empty($val)){
				$errors[$key] = $key.' is empty.';
			}
			$data[$key] = htmlspecialchars(trim($val));
		}
		$topic_id = $data['topic_id'];
		$page = $data['current_page'];

		if(!empty($errors)){
			return $this->showTopic($topic_id, $page, $errors);
		}

		$userName = htmlspecialchars( trim($data['author']) );
		$user = $userModel->getByName($userName);

		$postModel->addNewPost($this->request->getParams(), $user->getId());

		header('Location: /topic/show/'.$data['topic_id']);
	}

}