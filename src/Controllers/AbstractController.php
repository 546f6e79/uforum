<?php
namespace Uforum\Controllers;

use Interop\Container\ContainerInterface;
use Uforum\Core\Request;

abstract class AbstractController {
	protected $request;
	protected $di;
	protected $db;
	protected $config;
	protected $view;
	protected $log;

	public function __construct(ContainerInterface $di, Request $request) {
		$this->request    = $request;
		$this->di         = $di;
		$this->db         = $di->get('PDO');
		$this->view       = $di->get('Plates_Engine');
		$this->config     = $di->get('config');
		$this->userId     = isset($_COOKIE['id']) ? $_COOKIE['id'] : null;
	}

	/**
	 * Render tpl
	 * @param string $template
	 * @param array $params
	 * @return string
	 */
	protected function render(string $template, array $params): string {
		return $this->view->render($template, $params);
	}

}