<?php

namespace Uforum\Controllers;

class ErrorController extends AbstractController {

	/**
	 * 404 page
	 * @return string
	 */
	public function notFound(): string {
		http_response_code(404);
		$properties = ['errorMessage' => 'Page not found!'];
		return $this->render('error', $properties);
	}

}