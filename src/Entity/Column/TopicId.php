<?php

namespace Uforum\Entity\Column;

trait TopicId {
	/**
	 * @var integer
	 */
	private $topic_id;

	/**
	 * @return integer
	 */
	public function getTopicId() : int {
		return $this->topic_id;
	}

	/**
	 * @param integer $topic_id
	 */
	public function setTopicId($topic_id) {
		$this->topic_id = $topic_id;
	}

}