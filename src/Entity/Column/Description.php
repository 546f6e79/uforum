<?php

namespace Uforum\Entity\Column;

trait Description {
	/**
	 * @var string
	 */
	private $description;

	/**
	 * @return string
	 */
	public function getDescription() : string {
		return $this->description;
	}

	/**
	 * @param string $description
	 */
	public function setDescription($description) {
		$this->description = $description;
	}

}