<?php

namespace Uforum\Entity\Column;

trait Name {
	/**
	 * @var string
	 */
	private $name;

	/**
	 * @return string
	 */
	public function getName() : string {
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName($name) {
		$this->name = $name;
	}

}