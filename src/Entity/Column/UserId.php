<?php

namespace Uforum\Entity\Column;

trait UserId {
	/**
	 * @var integer
	 */
	private $user_id;

	/**
	 * @return integer
	 */
	public function getUserId() : int {
		return $this->user_id;
	}

	/**
	 * @param integer $user_id
	 */
	public function setUserId($user_id) {
		$this->user_id = $user_id;
	}

}