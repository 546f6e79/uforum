<?php

namespace Uforum\Entity\Column;

trait Id {
	/**
	 * @var integer
	 */
	private $id;

	/**
	 * @return integer
	 */
	public function getId() : int {
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId($id) {
		$this->id = $id;
	}

}