<?php

namespace Uforum\Entity\Column;

trait CreatedAt {
	/**
	 * @var string
	 */
	private $created_at;

	/**
	 * @return string
	 */
	public function getCreatedAt() : string {
		return $this->created_at;
	}

	/**
	 * @param string $created_at
	 */
	public function setCreatedAt($created_at) {
		$this->created_at = $created_at;
	}

}