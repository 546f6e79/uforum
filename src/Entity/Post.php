<?php

namespace Uforum\Entity;

use Uforum\Entity\Column\CreatedAt;
use Uforum\Entity\Column\Description;
use Uforum\Entity\Column\Id;
use Uforum\Entity\Column\Name;
use Uforum\Entity\Column\TopicId;
use Uforum\Entity\Column\UserId;

class Post {

	use Id;
	use TopicId;
	use UserId;
	use Name;
	use Description;
	use CreatedAt;

	/**
	 * @var string
	 */
	private $author;
	/**
	 * @var int
	 */
	private $count_posts_user;

	/**
	 * @return string
	 */
	public function getAuthor() : string {
		return $this->author;
	}

	/**
	 * @param string $author
	 */
	public function setAuthor($author) {
		$this->author = $author;
	}

	/**
	 * @return int
	 */
	public function getCountPostsUser() : int {
		return $this->count_posts_user;
	}

	/**
	 * @param int $count_posts_user
	 */
	public function setCountPostsUser($count_posts_user) {
		$this->count_posts_user = $count_posts_user;
	}

}