<?php

namespace Uforum\Entity;

use Uforum\Entity\Column\CreatedAt;
use Uforum\Entity\Column\Description;
use Uforum\Entity\Column\Id;
use Uforum\Entity\Column\Name;
use Uforum\Entity\Column\UserId;

class Topic {
	
	use Id;
	use UserId;
	use Name;
	use Description;
	use CreatedAt;

	/**
	 * @var string
	 */
	private $author;
	/**
	 * @var int
	 */
	private $posts_count;
	/**
	 * @var Post
	 */
	private $last_post;

	/**
	 * @return string
	 */
	public function getAuthor() : string {
		return $this->author;
	}

	/**
	 * @param string $author
	 */
	public function setAuthor($author) {
		$this->author = $author;
	}

	/**
	 * @return int
	 */
	public function getPostsCount() : int {
		return $this->posts_count;
	}

	/**
	 * @param int $posts_count
	 */
	public function setPostsCount($posts_count) {
		$this->posts_count = $posts_count;
	}

	/**
	 * @return Post
	 */
	public function getLastPost() : Post {
		return $this->last_post;
	}

	/**
	 * @param Post $last_post
	 */
	public function setLastPost($last_post) {
		$this->last_post = $last_post;
	}

}
