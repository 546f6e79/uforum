<?php

namespace Uforum\Entity;

use Uforum\Entity\Column\CreatedAt;
use Uforum\Entity\Column\Id;
use Uforum\Entity\Column\Name;

class User {

	use Id;
	use Name;
	use CreatedAt;

}