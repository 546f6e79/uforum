<?php
namespace Uforum\Factory;

use Interop\Container\ContainerInterface;

class PdoFactory {
	public function __invoke(ContainerInterface $container) {
		$config = $container->get('config')['database'];

		$dsn = sprintf(
			'mysql:host=%s;dbname=%s;charset=%s',
			$config['hostname'],
			$config['database'],
			$config['charset']
		);

		return new \PDO($dsn, $config['username'], $config['password'], $config['options']);
	}
}
