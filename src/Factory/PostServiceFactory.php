<?php

namespace Uforum\Factory;

use Interop\Container\ContainerInterface;
use Uforum\Service\Post;

class PostServiceFactory {
	public function __invoke(ContainerInterface $container) {
		$db = $container->get(\PDO::class);
		return new Post($db);
	}
}