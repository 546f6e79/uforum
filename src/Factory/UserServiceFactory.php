<?php

namespace Uforum\Factory;

use Interop\Container\ContainerInterface;
use Uforum\Service\User;

class UserServiceFactory {
	public function __invoke(ContainerInterface $container) {
		$db = $container->get(\PDO::class);
		return new User($db);
	}
}