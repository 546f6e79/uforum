<?php

namespace Uforum\Factory;

use Interop\Container\ContainerInterface;
use Uforum\Service\Topic;

class TopicServiceFactory {
	public function __invoke(ContainerInterface $container) {
		$db = $container->get(\PDO::class);
		return new Topic($db);
	}
}