<?php

namespace Uforum\Factory;

use Interop\Container\ContainerInterface;
use Uforum\Helper\Paginator;

class PaginatorFactory {
	public function __invoke(ContainerInterface $container) {
		return new Paginator();
	}
}