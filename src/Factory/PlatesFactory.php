<?php
namespace Uforum\Factory;

use Interop\Container\ContainerInterface;
use League\Plates\Engine;

class PlatesFactory {
	public function __invoke(ContainerInterface $container) {
		$config = $container->get('config')['views'];
		return new Engine($config);
	}
}
