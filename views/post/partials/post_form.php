<?php $this->insert('partials/form_flashes', ['errors' => $errors]) ?>
<div class="row">
	<div class="col-sm-offset-2 col-sm-8">
		<div class="panel panel-default">
			<div class="panel-heading">Reply</div>
			<div class="panel-body">
				<form class="form-horizontal white-box" action="/topic/post/create" method="post" name="topic">
					<div class="form-group">
						<div class="col-sm-12">
							<input name="author" type="text" class="form-control" placeholder="Your Name">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-12">
							<input name="name" type="text" class="form-control" placeholder="Post title">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-12">
							<textarea name="description" class="form-control" placeholder="Enter your message"></textarea>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-12 text-center">
							<input type="hidden" name="current_page" value="<?=$this->e($page)?>">
							<input type="hidden" name="topic_id" value="<?=$this->e($topic_id)?>">
							<button type="submit" class="btn btn-default">Add reply</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>