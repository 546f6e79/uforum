<?php
$colors = [
	'#0a4b91',
	'#8c013b',
	'#247003',
	'#08a087',
];
?>
<?php foreach ($posts as $post) { ?>
<div class="post-box replies">
	<div class="content-box-left">
		<div class="square">
			<div class="square-avatar">
				<span class="tavatar tip" style="background-color: <?php echo $colors[array_rand($colors)]; ?>;"><?php echo $post->getAuthor()[0]; ?></span>
			</div>
		</div>
		<div class="pinned-box">
			<div class="mt mb">
				<i class="tip fa fa-comments-o" title="Posts Counter for User"></i> <?=$post->getCountPostsUser(); ?>
			</div>
		</div>
	</div>
	<div class="content-box-md">
		<div class="col-xs-12">
			<div class="meta-box">
				<span class="label label-success"><i class="fa fa-user"></i> <?=$this->e($post->getAuthor());?></span> replied
				<div class="pull-right">
					<span class="label label-info"><i class="fa fa-clock-o"></i> <?=$this->e( date('d.m.Y H:i', strtotime($post->getCreatedAt())) );?></span>
				</div>
			</div>
			<div id="reply13">
				<?=$this->e($post->getDescription())?>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
</div>
<?php } ?>