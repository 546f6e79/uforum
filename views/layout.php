<html>
<head>
	<meta charset="UTF-8">
	<title><?=$this->e($title)?></title>
	<meta name="description" content="Simple forum engine">

	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="/css/style.css">
</head>
<body>
	<?=$this->insert('partials/header')?>
	<div class="content">
		<div class="container">
			<?=$this->section('content')?>
		</div>
	</div>
	<?=$this->insert('partials/footer')?>
</body>
</html>