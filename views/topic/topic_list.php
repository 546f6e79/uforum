<?php $this->layout('layout', ['title' => 'uForum - Topics']) ?>
<?php
$colors = [
	'#0a4b91',
	'#8c013b',
	'#247003',
	'#08a087',
];
?>

<div class="row">
	<?php foreach ($topics as $topic) { ?>
		<div class="col-sm-offset-2 col-sm-8">
			<div class="white-box content-box">
				<div class="content-box-left">
					<a data-id="3" class="user user_3" href="#">
						<div class="square">
							<div class="circle-avatar">
								<span class="avatar" style="background-color: <?php echo $colors[array_rand($colors)]; ?>;"><?php echo $topic->getAuthor()[0]; ?></span>
							</div>
						</div>
					</a>
				</div>
				<div class="content-box-middle">
					<h2><a href="/topic/show/<?=$this->e($topic->getId());?>"><?=$this->e($topic->getName());?></a></h2>
					<div class="meta-box">
						<span class="label label-info"><i class="fa fa-user"></i> Author: <?=$this->e($topic->getAuthor());?></span>
						<span class="label label-info"><i class="fa fa-clock-o"></i> <?=$this->e( date('d.m.Y H:i', strtotime($topic->getCreatedAt())) );?></span>
						<span class="label label-info"><i class="fa fa-comments-o"></i> <?=$this->e($topic->getPostsCount());?> Reply</span>
					</div>
					<h3>
						<?=$this->e($topic->getDescription()).'... <a href="/topic/show/'.$this->e($topic->getId()).'">read more</a>';?>
					</h3>
				</div>
				<div class="content-box-right">
					<?php if(!empty($topic->getLastPost()->getAuthor())){ ?>
						<h3 title="" class="tip" title="Last reply author"><i class="fa fa-reply"></i> Last reply: </h3>
						<h3 title="" class="tip" title="Last reply author"><i class="fa fa-user"></i> <?=$topic->getLastPost()->getAuthor();?> </h3>
						<h3 title="" class="tip" title="Last reply time"><i class="fa fa-clock-o"></i> <?=date('d.m.Y H:i', strtotime($topic->getLastPost()->getCreatedAt()));?></h3>
					<?php } ?>
				</div>
			</div>
		</div>
	<?php } ?>
	<?php if(empty($topics)){ ?>
	<div class="col-sm-offset-2 col-sm-8">
		<div class="alert alert-info text-center" role="alert">Forum is empty. Leave a message, to become the first author.</div>
	</div>
	<?php } ?>
</div>

<div class="row">
	<div class="col-sm-offset-2 col-sm-8 text-center">
		<?=$pagination;?>
	</div>
</div>

<?php $this->insert('topic/partials/topic_form', ['errors' => $errors]) ?>