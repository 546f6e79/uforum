<?php $this->layout('layout', ['title' => 'uForum - ' . $topic->getName()]) ?>
<?php
$colors = ['#0a4b91', '#8c013b', '#247003', '#08a087',];
?>

	<div class="row">
		<div class="col-sm-offset-2 col-sm-8">

			<div class="topic-box">
				<div class="content-box-top">
					<div class="square">
						<div class="square-avatar">
							<span class="tavatar tip"
								  style="background-color: <?php echo $colors[array_rand($colors)]; ?>;"><?php echo $topic->getAuthor()[0]; ?></span>
						</div>
					</div>
					<h2><?=$this->e($topic->getName());?></h2>
					<div class="meta-box">
						<span class="label label-info"><i class="fa fa-user"></i> Author: <?=$this->e($topic->getAuthor());?></span>
						<span class="label label-info"><i class="fa fa-clock-o"></i> <?=$this->e( date('d.m.Y H:i', strtotime($topic->getCreatedAt())) );?></span>
						<span class="label label-info"><i class="fa fa-comments-o"></i> <?=$this->e($topic->getPostsCount());?> Reply</span>
					</div>
				</div>
				<div class="content-box-md">
					<blockquote>
						<?=$this->e($topic->getDescription());?>
					</blockquote>
				</div>
				<hr>
			</div>

			<?php $this->insert('post/partials/posts_list', ['posts' => $posts]) ?>

		</div>
	</div>

	<div class="row">
		<div class="col-sm-offset-2 col-sm-8 text-center">
			<?=$pagination;?>
		</div>
	</div>

<?php $this->insert('post/partials/post_form', ['topic_id' => $topic->getId(), 'page' => $page, 'errors' => $errors]) ?>