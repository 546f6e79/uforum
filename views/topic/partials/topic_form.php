<?php $this->insert('partials/form_flashes', ['errors' => $errors]) ?>
<div class="row">
	<div class="col-sm-offset-2 col-sm-8">
		<div class="panel panel-default">
			<div class="panel-heading">Create new topic</div>
			<div class="panel-body">
				<form class="form-horizontal white-box" action="/topic/create" method="post" name="topic">
					<div class="form-group">
						<div class="col-sm-12">
							<input name="author" type="text" class="form-control" id="inputAuthor" placeholder="Your Name">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-12">
							<input name="name" type="text" class="form-control" id="inputTitle" placeholder="Topic Title">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-12">
							<textarea name="description" class="form-control" placeholder="Enter your message"></textarea>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-12 text-center">
							<button type="submit" class="btn btn-default">Create</button>
						</div>
					</div>
				</form>
			</div>
		</div>

	</div>
</div>