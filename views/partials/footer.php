<footer class="footer">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="footer-box">
					<div class="col-xs-4">
						<h2>2016 uForum Demo <small>(test case uralsoft)</small></h2>
					</div>
					<div class="col-xs-4">
						<h2><span>Simple forum engine powered by php7 + mysql</span></h2>
					</div>
					<div class="col-xs-4">
						<h2 class="text-right">Made by <a href="http://546f6e79.ninja/" target="_blank">546F6E79</a> | <a href="https://bitbucket.org/546f6e79/uforum/overview" target="_blank"><i class="fa fa-bitbucket"></i> Source</a></h2>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>