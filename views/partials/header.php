<nav class="navbar navbar-default">
	<div class="container">
		<div class="navbar-header">
			<div class="navbar-brand" id="logo">
				<h2>
					<a href="/">uForum</a>
				</h2>
			</div>
			<button aria-expanded="false" data-target="#bs-example-navbar-collapse-1" data-toggle="collapse"
					class="navbar-toggle collapsed" type="button">
				<span class="sr-only">Toggle Navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>
	</div>
</nav>