<?php if(!empty($errors)){ ?>
	<div class="row">
		<div class="alert alert-danger" role="alert">
			Form contains some errors:
			<ul>
				<?php foreach ($errors as $error){ ?>
					<li><?=$this->e($error)?></li>
				<?php } ?>
			</ul>
		</div>
	</div>
<?php } ?>