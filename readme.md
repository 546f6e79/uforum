Overview
--------

Simple forum engine powered by php7 + mysql

[demo](http://uforum.546f6e79.ninja/)

Deploy
------
```sh
$ # Install dependencies
$ composer install
$ # Setup db connection in config/app.php
$ cp config/app.php.sample config/app.php
$ # db schema in data/structure.sql
$ # start by built-in server for example
$ php -S localhost:3000 -t public/ public/index.php
```
